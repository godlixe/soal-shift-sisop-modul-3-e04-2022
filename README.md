# Penjelasan soal-shift-sisop-modul-3-e04-2022

### SOAL 1
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

a. Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

Download file zip quote dan music dengan `wget` dan diunzip kedalam folder masing-masing. Unzip dilakukan bersamaan menggunakan `thread` didalam fungsi downloadUnzipHandler().
```c
void downloadFiles(char *url, char *fileName){
    char download[100] = "https://docs.google.com/uc?export=download&id=";
    char tmp[100];
    strcpy(tmp, url);

    char *token = strtok(tmp, "/");
    while (token != NULL && !(strcmp(token, "d") == 0))
        token = strtok(NULL, "/");

    token = strtok(NULL, "/");
    strcat(download, token);

    char *argv[] = {"wget", "-q", "--no-check-certificate", download, "-O", fileName, NULL};
    execv("/usr/bin/wget", argv);
}

void unzip(char *fileName){
    char tmp[100];
    strcpy(tmp, fileName);
    char dirName[100];
    char *token = strtok(tmp, ".");
    strcpy(dirName, token);

    char *argv[] = {"unzip", fileName, "-d", dirName, NULL};
    execv("/usr/bin/unzip", argv);
}
void *downloadUnzipHandler(){
    char *url[2] = {"https://drive.google.com/file/d/1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1/view", "https://drive.google.com/file/d/1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt/view"};

    pthread_t id = pthread_self();
    if (pthread_equal(id, tid[0])){
        int status;
        child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                downloadFiles(url[0], "music.zip");

            else{
                while ((wait(&status)) > 0);
                unzip("music.zip");
            }
        }
    }

    else if (pthread_equal(id, tid[1])){
        int status;
        child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                downloadFiles(url[1], "quote.zip");

            else{
                while ((wait(&status)) > 0);
                unzip("quote.zip");
            }
        }
    }
    return NULL;
}

```

b. Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

Decode semua file .txt dengan base64 dari setiap folder menjadi masing-masing satu file .txt untuk setiap folder secara bersamaan menggunakan `thread`. 

```c
#define WHITESPACE 64
#define EQUALS 65
#define INVALID 66

static const unsigned char d[] = {
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 64, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 62, 66, 66, 66, 63, 52, 53,
    54, 55, 56, 57, 58, 59, 60, 61, 66, 66, 66, 65, 66, 66, 66, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
    10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 66, 66, 66, 66, 66, 66, 26, 27, 28,
    29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66};

int base64decode(char *in, size_t inLen, unsigned char *out, size_t *outLen){
    char *end = in + inLen;
    char iter = 0;
    uint32_t buf = 0;
    size_t len = 0;

    while (in < end){
        unsigned char c = d[*in++];

        switch (c){
        case WHITESPACE:
            continue; /* skip whitespace */
        case INVALID:
            return 1; /* invalid input, return error */
        case EQUALS:  /* pad character, end of data */
            in = end;
            continue;
        default:
            buf = buf << 6 | c;
            iter++; // increment the number of iteration
            /* If the buffer is full, split it into bytes */
            if (iter == 4){
                if ((len += 3) > *outLen)
                    return 1; /* buffer overflow */
                *(out++) = (buf >> 16) & 255;
                *(out++) = (buf >> 8) & 255;
                *(out++) = buf & 255;
                buf = 0;
                iter = 0;
            }
        }
    }

    if (iter == 3){
        if ((len += 2) > *outLen)
            return 1; /* buffer overflow */
        *(out++) = (buf >> 10) & 255;
        *(out++) = (buf >> 2) & 255;
    }
    else if (iter == 2){
        if (++len > *outLen)
            return 1; /* buffer overflow */
        *(out++) = (buf >> 4) & 255;
    }

    *outLen = len; /* modify to reflect the actual output size */
    return 0;
}
void decodeFiles(char *name){
    struct dirent **namelist;
    int n;

    n = scandir(name, &namelist, NULL, alphasort);
    int i = 0;
    if (n == -1)
        perror("scandir");

    for (int i = 0; i < n; i++){
        if (strcmp(namelist[i]->d_name, ".") != 0 && strcmp(namelist[i]->d_name, "..") != 0){
            char *decoded;
            char dirName[100];
            strcpy(dirName, name);
            strcat(dirName, "/");
            strcat(dirName, namelist[i]->d_name);
            free(namelist[i]);

            FILE *fp = fopen(dirName, "r");
            char line[100];
            while (fgets(line, 100, fp) != NULL){
                char *in = line;
                size_t inLen = strlen(in);
                size_t outLen = inLen;
                decoded = malloc(sizeof(char) * 32768);
                base64decode(in, inLen, (unsigned char *)decoded, &outLen);
            }
            fclose(fp);

            char txtDir[100];
            strcpy(txtDir, name);
            strcat(txtDir, "/");
            strcat(txtDir, name);
            strcat(txtDir, ".txt");

            fp = fopen(txtDir, "a");
            fprintf(fp, "%s\n", decoded);
            fclose(fp);
        }
    }
    free(namelist);
}

void *decodeHandler(){
    pthread_t id = pthread_self();
    if (pthread_equal(id, tid[2]))
        decodeFiles("music");
    
    else if (pthread_equal(id, tid[3]))
        decodeFiles("quote");

    return NULL;
}

```

c. Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.

Pindahkan masing-masing file .txt hasil decoding ke dalam folder baru bernama hasil. Menggunakan `execv` untuk `mkdir` folder hasil dan `mv` file .txt. Digunakan thread berisi fungsi moveFileHandler().

```c
void mkDir(char *name){
    char *argv[] = {"mkdir", "-p", name, NULL};
    execv("/usr/bin/mkdir", argv);
}

void moveFile(char *fileName){
    char src[100];
    strcpy(src, fileName);
    strcat(src, "/");
    strcat(src, fileName);
    strcat(src, ".txt");

    char dest[100];
    strcpy(dest, "hasil/");
    strcat(dest, fileName);
    strcat(dest, ".txt");

    char *argv[] = {"mv", src, dest, NULL};
    execv("/bin/mv", argv);
}

void *moveFileHandler(){
    pthread_t id = pthread_self();
    if (pthread_equal(id, tid[4])){
        int status;
        child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                mkDir("hasil");

            else{
                while ((wait(&status)) > 0);
                moveFile("music");
            }
        }
    }
    else if (pthread_equal(id, tid[5])){
        int status;
        child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                mkDir("hasil");

            else{
                while ((wait(&status)) > 0);
                moveFile("quote");
            }
        }
    }
    return NULL;
}

```

d. Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)

e. Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

Setelah folder hasil di zip denggan pass yang ditentukan ternyata ada yang kurang dan diunzip kembali lalu ditambahkan file no.txt berisi 'NO'. Lalu di zip kembali seperti no.4.

```c
void unzipWithPassword(char *fileName, char *password){
    char tmp[100];
    strcpy(tmp, fileName);
    char dirName[100];
    char *token = strtok(tmp, ".");
    strcpy(dirName, token);

    char *argv[] = {"unzip", "-P", password, fileName, "-d", dirName, NULL};
    execv("/usr/bin/unzip", argv);
}

void zip(char *name, char *password){
    char fileName[100];
    strcpy(fileName, name);
    strcat(fileName, ".zip");

    char src[100];
    strcpy(src, name);
    strcat(src, "/*");

    char *argv[] = {"zip", "-P", password, "-r", "-j", fileName, name, NULL};
    execv("/usr/bin/zip", argv);
}

void deleteDir(char *name){
    char *argv[] = {"rm", "-rf", name, NULL};
    execv("/bin/rm", argv);
}

void addFile(char *name){
    FILE *fp = fopen(name, "w");
    fprintf(fp, "No\n");
    fclose(fp);
}

void *zipHandler(){
    char password[100] = "mihinomenest";
    char *user = getenv("USER");
    strcat(password, user);

    pthread_t id = pthread_self();
    if (pthread_equal(id, tid[6])){
        int status;
        child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                zip("hasil", password);

            else{
                while ((wait(&status)) > 0);
                pid_t child_id2 = fork();

                if (child_id2 == 0)
                    deleteDir("hasil");

                else{
                    while ((wait(&status)) > 0);
                    pid_t child_id3 = fork();

                    if (child_id3 == 0)
                        unzipWithPassword("hasil.zip", password);

                    else{
                        while ((wait(&status)) > 0);
                        addFile("hasil/no.txt");
                        zip("hasil", password);
                    }
                }
            }
        }
    }
    return NULL;
}
```


### SOAL 2
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:
Username unique (tidak boleh ada user yang memiliki username yang sama)
Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil
Format users.txt:

users.txt
username:password
username2:password2


Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan \t. File otomatis dibuat saat server dijalankan.

Client yang telah login, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)
	Contoh:
	Client-side
add

	
	Server-side
	
Judul problem:
Filepath description.txt:
Filepath input.txt:
Filepath output.txt:


	Client-side
	
<judul-problem-1>
<Client/description.txt-1>
<Client/input.txt-1>
<Client/output.txt-1>


Seluruh file akan disimpan oleh server ke dalam folder dengan nama <judul-problem> yang di dalamnya terdapat file description.txt, input.txt dan output.txt. Penambahan problem oleh client juga akan mempengaruhi file problems.tsv.

Client yang telah login, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). Format yang akan ditampilkan oleh server adalah sebagai berikut:
```
<judul-problem-1> by <author1>
<judul-problem-2> by <author2>
```

Client yang telah login, dapat memasukkan command ‘download <judul-problem>’ yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu <judul-problem>. Kedua file tersebut akan disimpan ke folder dengan nama <judul-problem> di client.

Client yang telah login, dapat memasukkan command ‘submit <judul-problem> <path-file-output.txt>’.  Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”.

Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.

## JAWABAN
Pertama, dibuat program server.c untuk menghandle request dari client. Pada program server.c, saya membuat beberapa fungsi untuk membantu kerja program. Saya menggunakan program template dari modul untuk koneksinya dan membuat fungsi lainnya dengan mempelajari modul.

```
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#define PORT 8080

int startServer(struct sockaddr_in address, int addrlen)
{
    int fd, opt = 1;
    if ((fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    return fd;
}

int validateUsername(FILE *fptr, char *username)
{
    char buffer[200];
    while (fscanf(fptr, "%s", buffer) != EOF)
    {
        char *token = strtok(buffer, ":");
        if (!strcmp(username, token))
        {
            return 1;
        }
    }
    return 0;
}

int validatePassword(char *password)
{
    // check for length, number, uppercase, and lowercase character
    int c_num = 0, c_upp = 0, c_low = 0;
    if (strlen(password) < 6)
        return 1;
    for (int i = 0; password[i] != '\0'; i++)
    {
        c_num = c_num || isdigit(password[i]);
        c_upp = c_upp || (isalpha(password[i]) && password[i] == toupper(password[i]));
        c_low = c_low || (isalpha(password[i]) && password[i] == tolower(password[i]));
    }
    if (!(c_num && c_upp && c_low))
        return 1;
    return 0;
}

void handleRegister(int fd, char *username)
{
    FILE *fptr;
    fptr = fopen("users.txt", "a+");
    char password[100];
    if (validateUsername(fptr, username) != 1)
    {
        send(fd, "Masukkan password kamu!", 100, 0);
        read(fd, password, 1024);
        if (validatePassword(password))
        {
            send(fd, "Password belum sesuai persyaratan!", 100, 0);
        }
        printf("uname %s\n", username);
        printf("pass %s\n", password);
        fprintf(fptr, "%s:%s\n", username, password);
        fclose(fptr);
        return;
    }
    else
    {
        send(fd, "Username sudah ada dalam database!", 100, 0);
        close(fd);
        exit;
    }
}

void handleLogin(int fd, char *username)
{
    char password[100];
    send(fd, "Masukkan password kamu!", 100, 0);
    read(fd, password, 1024);
    FILE *fptr = fopen("users.txt", "a+");
    char buffer[100], comp[201];
    snprintf(comp, 201, "%s:%s", username, password);
    while (fscanf(fptr, "%s", buffer) != EOF)
    {
        if (!strcmp(comp, buffer))
        {
            fclose(fptr);
            return;
        }
    }
    send(fd, "Login gagal!", 100, 0);
    fclose(fptr);
}

int validateProblem(char *title)
{
    FILE *tsv = fopen("problems.tsv", "r");
    char buffer[200];
    while (fgets(buffer, 150, tsv))
    {
        buffer[strcspn(buffer, "\n")] = 0;
        char *token = strtok(buffer, "\t");
        if (!strcmp(title, token))
        {
            return 1;
        }
    }
    fclose(tsv);
    return 0;
}

void copyFiles(char *sc, char *dest)
{
    FILE *fp1 = fopen(sc, "a+");
    FILE *fp2 = fopen(dest, "a+");
    char c;
    c = fgetc(fp1);
    while (c != EOF)
    {
        fputc(c, fp2);
        c = fgetc(fp1);
    }
    fclose(fp1);
    fclose(fp2);
}

void handleAddProblem(int fd, char *username)
{
    FILE *tsv = fopen("problems.tsv", "a+");
    char judul[150], f_desc[150], f_inp[150], f_out[150], c;
    send(fd, "Masukkan judul problem : ", 150, 0);
    read(fd, judul, 1024);
    if (validateProblem(judul))
    {
        send(fd, "re_inp", 150, 0);
        send(fd, "Judul sudah ada, silakan masukkan ulang", 150, 0);
        sleep(1);
        handleAddProblem(fd, username);
        return;
    }
    send(fd, "Masukkan filepath deskripsi problem : ", 150, 0);
    read(fd, f_desc, 1024);
    send(fd, "Masukkan filepath input problem : ", 150, 0);
    read(fd, f_inp, 1024);
    send(fd, "Masukkan filepath output problem : ", 150, 0);
    read(fd, f_out, 1024);
    fprintf(tsv, "%s\t%s\n", judul, username);
    fclose(tsv);
    mkdir(judul, 0777);
    chdir(judul);
    copyFiles(f_desc, "description.txt");
    copyFiles(f_inp, "input.txt");
    copyFiles(f_out, "output.txt");
}

void handleSeeProblem(int fd, char *username)
{
    char cwd[200];
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        printf("Current working dir: %s\n", cwd);
    }
    FILE *tsv = fopen("problems.tsv", "a+");
    char buffer[1000], c;
    c = fgetc(tsv);
    while (c != EOF)
    {
        if (c == '\t')
        {
            strcat(buffer, " by ");
        }
        else
        {
            strncat(buffer, &c, 1);
        }
        c = fgetc(tsv);
    }
    send(fd, buffer, 1000, 0);
    return;
}

void handleDownload(int fd, char *judul)
{
    char buffer[90], filename1[100] = "../../Client/", filename2[100] = "../../Client/", dirname[100] = "../../Client/";
    for (int i = 9; i < strlen(judul) + 1; i++)
    {
        buffer[i - 9] = judul[i];
    }
    chdir(buffer);
    strcat(dirname, buffer);
    strcat(filename1, buffer);
    strcat(filename1, "/description.txt");
    strcat(filename2, buffer);
    strcat(filename2, "/input.txt");
    mkdir(dirname, 0777);
    copyFiles("description.txt", filename1);
    copyFiles("input.txt", filename2);
}

void handleSubmit(int fd, char *judul)
{
    char cwd[200];
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        printf("Current working dir: %s\n", cwd);
    }
    printf("%s\n", judul);
    int p_f = 0, ln = 0;
    char buffer[100], filename1[100], filename2[100], c, c_sub;
    for (int i = 7; i <= strlen(judul); i++)
    {
        buffer[i - 7] = judul[i];
    }
    for (int i = 0; i <= strlen(buffer) + 1; i++)
    {
        // printf("ini strlen buffer %lu\n", strlen(buffer) - 2);
        if (judul[i] == ' ')
        {
            filename1[i] = '\0';
            p_f = 1;
        }
        else if (!p_f)
        {
            filename1[i] = buffer[i];
        }
        else
        {
            filename2[ln] = buffer[i];
            // printf("ini fp sub %s\n", filename2);
            ln++;
        }
    }
    filename2[ln + 1] = '\0';
    printf("ini soal %s\n", filename1);
    printf("ini fp sub %s\n", filename2);
    chdir(filename1);
    FILE *f_inp = fopen("output.txt", "a+"), *f_sub = fopen(filename2, "a+");
    c = fgetc(f_inp);
    c_sub = fgetc(f_sub);
    while (c != EOF)
    {
        if (c != c_sub)
        {
            send(fd, "WA", 5, 0);
            return;
        }
        c_sub = fgetc(f_sub);
        c = fgetc(f_inp);
    }
    fclose(f_inp);
    fclose(f_sub);
    send(fd, "AC", 5, 0);
}

void *handleConnection(void *argv)
{
    int new_socket = *((int *)argv), auth = 0;
    free(argv);
    char response_buffer[1024], cmd_buffer[1024], username[100];
    if (!auth)
    {
        send(new_socket, "Please enter an option : [register/login]", 100, 0);
        read(new_socket, response_buffer, 1024);
        send(new_socket, "Masukkan username kamu!", 100, 0);
        read(new_socket, username, 1024);
        if (!strcmp(response_buffer, "register"))
        {
            handleRegister(new_socket, username);
        }
        else
        {
            handleLogin(new_socket, username);
        }
        auth = 1;
    }
    while (auth)
    {
        char cwd[200];
        if (getcwd(cwd, sizeof(cwd)) != NULL)
        {
            printf("Current working dir: %s\n", cwd);
        }
        chdir("home/alex/soal-shift-sisop-modul-3-E04-2022/soal2/Server");
        send(new_socket, "Berhasil masuk. Masukkan perintah [add/see/download <judul-problem>/submit <judul-problem> <path-file-output.txt>]", 150, 0);
        read(new_socket, cmd_buffer, 1024);
        if (!strcmp(cmd_buffer, "add"))
        {
            handleAddProblem(new_socket, username);
        }
        else if (!strcmp(cmd_buffer, "see"))
        {
            handleSeeProblem(new_socket, username);
        }
        else if (cmd_buffer[0] == 'd')
        {
            handleDownload(new_socket, cmd_buffer);
        }
        else if (cmd_buffer[0] == 's')
        {
            handleSubmit(new_socket, cmd_buffer);
        }
        else
        {
            close(new_socket);
        }
    }

    return NULL;
}

int main(int argc, char const *argv[])
{
    struct sockaddr_in address;
    int addrlen = sizeof(address), new_socket, s_count = 0;
    while (1)
    {
        if (!s_count)
        {
            int fd = startServer(address, addrlen);
            if ((new_socket = accept(fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
            {
                perror("accept");
                exit(EXIT_FAILURE);
            }
            pthread_t t;
            int *p_client = malloc(sizeof(int));
            *p_client = new_socket;
            pthread_create(&t, NULL, &handleConnection, p_client);
            s_count++;
        }
    }
    return 0;
}
```

Untuk client.c saya juga menggunakan template kode dari modul dan memodifikasinya dengan menambahkan beberapa fungsi pembantu.

```
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080

int startClient(struct sockaddr_in serv_addr)
{
    int fd;
    if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }
    return fd;
}

int main()
{
    struct sockaddr_in serv_addr;
    int fd = startClient(serv_addr), valread;
    while (1)
    {
        fflush(stdin);
        char response_buffer[1024] = {0}, buffer[100] = {0};
        read(fd, response_buffer, 1024);
        // Case if server sends a reinput
        if (!strcmp(response_buffer, "re_inp"))
        {
            printf("%s\n", response_buffer);
            memset(response_buffer, 0, strlen(response_buffer));
            read(fd, response_buffer, 1024);
            printf("%s\n", response_buffer);
            memset(response_buffer, 0, strlen(response_buffer));
            read(fd, response_buffer, 1024);
        }
        printf("%s\n", response_buffer);
        fgets(buffer, 200, stdin);
        buffer[strcspn(buffer, "\n")] = 0;
        // scanf("%s", buffer);
        send(fd, buffer, 100, 0);
    }

    return 0;
}
```

## kesulitan : 
1. Kesulitan dalam melakukan manipulasi string pada C
2. Kesulitan dalam menerima pesan karena fungsi read/recv tidak tentu.

## soal no 3
Pada soal no 3 ini, diminta untuk mencoba merapikan file bernama harta karun berdasarkan jenis/tipe/kategori/ekstensinya. mengextract zip yang diberikan dalam folder /home/[user]/shift3/. Lalu working directory program akan berada dalam folder /home/[user]/shift3/hartakarun/. Semua file harus berada dalam folder, jika terdapat file yang tidak memiliki eksistensi, maka file akan disimpan dalam folder unknown jika file hidden makan akan disimpan dalam folder hidden. Agara proses kategori dapat berjalan dengan cepat, maka setiap 1 file yang dikategorikan akan dioperasika oleh 1 thread. Nami menggunakan progra client-server. Saat progra client dijalan kan , maka folder /home/[user]/shift3/hartakarun/akan di-zip terlebih dhulu. client dapat mengirim file hartakarun.zip ke server.

A. Extract Zip
Pertama akan membuat void extract, dengan menggunakan fungsi popen dengan argumen pertamaya adalah value dan argumen kedua adalah r.
```
void extract(char *value) 
{
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer,'\0',sizeof(buffer));

    file = popen(value ,"r");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n",buffer);
        pclose(file);
    }
}
```




B. Kategori file Unknown dan Hidden
Pertama akan membuat void sortfile untuk mengkategorikan file
```
void sortFile(char *from)
{
    struct_path s_path;
    struct dirent *dp;
    DIR *dir;

    flag = 1;
    int index = 0;
    strcpy(s_path.cwd, "/home/theresianwg/shift3/hartakarun");

    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);

            pthread_create(&tid[index], NULL, move, (void *)&s_path);
            sleep(1);
            index++;
        }
    }
}
```

lalu program akan membaca directory hasil extract file.zip dan akan dicek satu-satu. Untuk informasi mengenai directory asal dan nama file akan disimpan pada variabel filename




C. Kategori File menggunakan Thread
Pada kali ini menggunakan fungsi sortfile dan memamnggil fungsi move
```
pthread_create(&tid[index], NULL, move, (void *)&s_path);


lalu menggunakan

void *move(void *s_path)
{
    struct_path s_paths = *(struct_path*) s_path;
    moveFile(s_paths.from, s_paths.cwd);
    pthread_exit(0);
}
```


dan dilanjutkan dengan void movefile
```
void moveFile(char *p_from, char *p_cwd)
{
    char file_name[300], file_ext[30], temp[300];
    char *buffer;

    strcpy(temp, p_from);
    buffer = strtok(temp, "/");

    while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }

    toLower(file_ext);

    // new dest
    strcat(p_cwd, "/");
    strcat(p_cwd, file_ext);
    strcat(p_cwd, "/");
    strcat(p_cwd, file_name);

    // information
    printf("from %s: \nfile_name: %s\nfile_ext: %s\ndest: %s\n\n", p_from, file_name, file_ext, p_cwd);

    // create dir and move file
    createDir(file_ext);
    rename(p_from, p_cwd);
}
```

dengan bantuan variabel buffer yang sebelumnya akan diisi nama file dengan bantuan fungsi strtok untum mencari delimiter dengn /. Lalu, menentukan eksistensi file dengan bantuan variabel buffer  yang berisi formt file stelah diinisiasi hasil dari strtok dengan delimiter .. Tetapi, terdapat pengecualian jika NULL maka artinya file dikategorikan hidden dan jika tidak memenehui keduannya akan dikategorikan unkown. Dilanjutkan dengan membuat destinasi dari file, juga menampilkan informasi file dan membuat directory berdasarkan eksistensi filenya.


D. Folder akan di Zip (hartakarun.zip)
Disini akan menggunakan void zip yang akan menerima parameter dengan berupa command yang dieksekusi pada terminal. Command ini selanjutnya akan dieksekusi dengan popen yang ada dalam fungsi zip
```
void zip(char *ziping)
{
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer, '\0', sizeof(buffer));

    //ziping
    file = popen(ziping, "w");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n", buffer);
        pclose(file);
    }
}
```


##Kendala Soal no 3
file pada folder hidden kosong
